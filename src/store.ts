import { configureStore } from '@reduxjs/toolkit';
import { filmsPageSlice, FilmsPageState } from './FilmsPage/filmsPageSlice';
import { filmReviewPageSlice, FilmReviewPageState } from './FilmReviewPage/filmReviewPageSlice';

/** Состояние приложения. */
export interface State {
  filmsPage: FilmsPageState,
  filmReviewPage: FilmReviewPageState
}

export default configureStore({
  reducer: {
    filmsPage: filmsPageSlice.reducer,
    filmReviewPage: filmReviewPageSlice.reducer
  },
});

export const selectFilmsPageState = (state: State) => state.filmsPage;
export const selectFilmReviewPageState = (state: State) => state.filmReviewPage;
