import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { Film } from '../common';
import { filmService } from '../services';

/** Статус блока со списком. */
export enum FilmsPageListStatus {
  Initial,
  Loading,
  Loaded,
  Failed
}

/** Статус карточки фильма. */
export enum FilmsPageCardStatus {
  Initial,
  Loading,
  Loaded,
  Failed
}

/** Состояние страницы со списком фильмов. */
export interface FilmsPageState {
  list: {
    status: FilmsPageListStatus;
    currentRequestId: string | null;
    films: Film[];
  };
  card: {
    status: FilmsPageCardStatus;
    currentRequestId: string | null;
    film: Film | null;
  };
}

/** Thunk для загрузки блока со списком. */
export const loadList = createAsyncThunk(
  'filmsPage/loadList',
  async () => filmService.getFilms()
);

/** Thunk для загрузки блока со карточкой фильма. */
export const loadCard = createAsyncThunk(
  'filmsPage/loadCard',
  async (filmId: string) => filmService.getFilm(filmId)
);

/** Slice страницы со списком фильмов. */
export const filmsPageSlice = createSlice({
  name: 'filmsPage',
  initialState: {
    list: {
      status: FilmsPageListStatus.Initial,
      currentRequestId: null,
      films: []
    },
    card: {
      status: FilmsPageCardStatus.Initial,
      currentRequestId: null,
      film: null
    }
  } as FilmsPageState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(loadList.pending, (state, action) => {
      state.list.status = FilmsPageListStatus.Loading;
      state.list.currentRequestId = action.meta.requestId;
    });

    builder.addCase(loadList.fulfilled, (state, action) => {
      if (action.meta.requestId !== state.list.currentRequestId) {
        return;
      }

      state.list.status = FilmsPageListStatus.Loaded;
      state.list.currentRequestId = null;
      state.list.films = action.payload;
    });

    builder.addCase(loadList.rejected, (state, action) => {
      if (action.meta.requestId !== state.list.currentRequestId) {
        return;
      }

      state.list.status = FilmsPageListStatus.Failed;
      state.list.currentRequestId = null;
    });

    builder.addCase(loadCard.pending, (state, action) => {
      state.card.status = FilmsPageCardStatus.Loading;
      state.card.currentRequestId = action.meta.requestId;
    });

    builder.addCase(loadCard.fulfilled, (state, action) => {
      if (action.meta.requestId !== state.card.currentRequestId) {
        return;
      }

      state.card.status = FilmsPageCardStatus.Loaded;
      state.card.currentRequestId = null;
      state.card.film = action.payload;
    });

    builder.addCase(loadCard.rejected, (state, action) => {
      if (action.meta.requestId !== state.card.currentRequestId) {
        return;
      }

      state.card.status = FilmsPageCardStatus.Failed;
      state.card.currentRequestId = null;
    });
  },
});
