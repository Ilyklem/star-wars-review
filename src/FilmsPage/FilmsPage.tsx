import React, { useEffect } from 'react';
import { useRouteMatch } from 'react-router';
import FilmCard from './components/FilmCard';
import { Col, ListGroup, Row } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useLocation } from 'react-router-dom';
import { FilmsPageListStatus } from './filmsPageSlice';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from '@reduxjs/toolkit';
import { selectFilmsPageState } from '../store';
import { loadList } from './filmsPageSlice';

import styles from './FilmsPage.module.css';

const selectStatus = createSelector([selectFilmsPageState], (state) => state.list.status);
const selectFilms = createSelector([selectFilmsPageState], (state) => state.list.films);

/** Компонент страницы со списком фильмов. */
export default function FilmsPage() {
  const match = useRouteMatch<{ filmId: string | undefined }>();
  const { pathname } = useLocation();

  const dispatch = useDispatch();
  const status = useSelector(selectStatus);
  const films = useSelector(selectFilms);

  useEffect(() => {
    dispatch(loadList());
  }, [dispatch]);

  const selectedFilmId = match.params.filmId ?? null;

  return <>
    {status === FilmsPageListStatus.Loaded &&
      <Row>
          <Col sm={4}>
            {/* Should always rerender on location change due to:
                https://github.com/react-bootstrap/react-router-bootstrap/issues/242 */}
            <ListGroup key={pathname}>
              {films.map(film =>
                <LinkContainer key={film.id} to={`/${film.id}`}>
                  <ListGroup.Item action>
                    {film.title}
                  </ListGroup.Item>
                </LinkContainer>
              )}
            </ListGroup>
          </Col>
          <Col className={styles.cardColumn} sm={8}>
            {selectedFilmId !== null && <FilmCard/>}
          </Col>
      </Row>
    }
    {status === FilmsPageListStatus.Loading && 'Loading…'}
    {status === FilmsPageListStatus.Failed && 'Unknown error'}
  </>;
}
