import React, { useEffect } from 'react';
import { Button, Card } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useRouteMatch } from 'react-router';
import { createSelector } from '@reduxjs/toolkit';
import { selectFilmsPageState } from '../../store';
import { useDispatch, useSelector } from 'react-redux';
import { FilmsPageCardStatus, loadCard } from '../filmsPageSlice';

import image from './image.png';

const selectStatus = createSelector([selectFilmsPageState], (state) => state.card.status);
const selectFilm = createSelector([selectFilmsPageState], (state) => state.card.film);

/** Компонент карточки фильма. */
export default function FilmCard() {
  const match = useRouteMatch<{ filmId: string }>();

  const dispatch = useDispatch();
  const status = useSelector(selectStatus);
  const film = useSelector(selectFilm);

  useEffect(() => {
    dispatch(loadCard(match.params.filmId));
  }, [match.params.filmId, dispatch]);

  return (
    <Card>
      <Card.Img variant="top" src={image} />
      <Card.Body>
        {status === FilmsPageCardStatus.Loaded && (
          film !== null ?
            <>
              <Card.Title>{film.title}</Card.Title>
              <Card.Text>{film.openingCrawl}</Card.Text>
              <LinkContainer to={`/${match.params.filmId}/review`}>
                <Button>Leave a review</Button>
              </LinkContainer>
            </>
            : 'Unknown film'
        )}
        { status === FilmsPageCardStatus.Loading && 'Loading...'}
        { status === FilmsPageCardStatus.Failed && 'Unknown error'}
      </Card.Body>
    </Card>
  );
}
