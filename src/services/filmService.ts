import { FilmService } from '../common';

/** Экземпляр сервиса для работа с библиотекой фильмов по вселенной «Star Wars». */
export const filmService = new FilmService();
