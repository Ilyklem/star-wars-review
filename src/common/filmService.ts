/** Фильм по вселенной «Star Wars». */
export interface Film {
  id: string;
  title: string;
  openingCrawl: string;
}

/** Сервис для работа с библиотекой фильмов по вселенной «Star Wars». */
export class FilmService {
  private static readonly basePath = '//swapi.dev/api';

  /** Возвращает список фильмов. */
  async getFilms(): Promise<Film[]> {
    const path = `${FilmService.basePath}/films/`;

    const response = await fetch(path);

    if (response.ok) {
      const json = await response.json();
      const films = json.results as any[];

      return films.map((film, index) => ({
        // В качестве ID фильма в API используется индекс фильма, начиная с 1.
        id: String(index + 1),
        title: film.title,
        openingCrawl: film.opening_crawl
      }));
    } else {
      throw new Error('Unknown error.');
    }
  }

  /**
   * Возвращает данные конкретного фильма по его ID.
   *
   * @param filmId ID фильма
   */
  async getFilm(filmId: string): Promise<Film | null> {
    const path = `${FilmService.basePath}/films/${filmId}/`;

    const response = await fetch(path);

    if (response.ok) {
      const film = await response.json();

      return {
        id: filmId,
        title: film.title,
        openingCrawl: film.opening_crawl
      };
    } else {
      if (response.status === 404) {
        return null;
      } else {
        throw new Error('Unknown error.');
      }
    }
  }

  /**
   * Создаёт рецензию на фильм с ID `filmId`.
   *
   * @param filmId ID фильма
   * @param username имя пользователя
   * @param email email пользователя
   * @param text текст рецензии
   */
  async createReview(filmId: string, username: string, email: string, text: string) {
    return new Promise(resolve => setTimeout(resolve, 1000));
  }
}
