import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { filmService } from '../services';

/** Статус страницы для создания рецензии на фильм. */
export enum FilmReviewPageStatus {
  Initial,
  Submitting,
  Done,
  Failed
}

/** Набор данных для создания рецензии на фильм. */
export interface FilmReviewPageData {
  username: string;
  email: string;
  text: string;
}

/** Состояние страницы для создания рецензии на фильм. */
export interface FilmReviewPageState {
  status: FilmReviewPageStatus;
  lastSubmittedData: FilmReviewPageData | null;
  currentRequestId: string | null;
}

export interface SubmitPayload {
  filmId: string;
  data: FilmReviewPageData;
}

/** Thunk для создания рецензии. */
export const submit = createAsyncThunk(
  'filmReviewPage/submit',
  async ({filmId, data}: SubmitPayload) => {
    await filmService.createReview(filmId, data.username, data.email, data.text);
    return data;
  }
);

/** Slice страницы для создания рецензии на фильм. */
export const filmReviewPageSlice = createSlice({
  name: 'filmReviewPage',
  initialState: {
    status: FilmReviewPageStatus.Initial,
    lastSubmittedData: null,
    currentRequestId: null,
  } as FilmReviewPageState,
  reducers: {
    /** Действие для сброса страницы в начальное состояние. */
    reset: (state) => {
      state.status = FilmReviewPageStatus.Initial;
      state.lastSubmittedData = null;
      state.currentRequestId = null;
    }
  },
  extraReducers: builder => {
    builder.addCase(submit.pending, (state, action) => {
      state.status = FilmReviewPageStatus.Submitting;
      state.lastSubmittedData = null;
      state.currentRequestId = action.meta.requestId;
    });

    builder.addCase(submit.fulfilled, (state, action) => {
      if (action.meta.requestId !== state.currentRequestId) {
        return;
      }

      state.status = FilmReviewPageStatus.Done;
      state.lastSubmittedData = action.payload;
      state.currentRequestId = null;
    });

    builder.addCase(submit.rejected, (state, action) => {
      if (action.meta.requestId !== state.currentRequestId) {
        return;
      }

      state.status = FilmReviewPageStatus.Failed;
      state.currentRequestId = null;
    });
  },
});

export const { reset } = filmReviewPageSlice.actions;
