import { useField } from 'formik';
import { Form } from 'react-bootstrap';
import React, { ReactNode } from 'react';

export interface FormFieldProps {
  /** Заголовок. */
  label: ReactNode;
  /** Название поля. Будет использовано для получения данных из Formik. */
  name: string;
  /** Тип инпута, который будет отрендерен. Если не указывать — отрендерится `input`. */
  inputType?: React.ElementType;
  /** Флаг заблокированности поля. */
  disabled: boolean;
}

/** Компонент поля ввода, инкапсулирующий в себя логику работы с Formik. */
export default function FormField({label, name, inputType, disabled}: FormFieldProps) {
  const [{onChange, onBlur, value}, {error, touched}] = useField(name);

  return (
    <Form.Group controlId={name}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        as={inputType}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        value={value}
        disabled={disabled}
        isInvalid={error !== undefined && touched}
      />
      <Form.Control.Feedback type="invalid">{error}</Form.Control.Feedback>
    </Form.Group>
  );
};
