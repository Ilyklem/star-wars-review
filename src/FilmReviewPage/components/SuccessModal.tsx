import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { FilmReviewPageData } from '../filmReviewPageSlice';

export interface SuccessModalProps {
  /** Флаг для отображения модального окна. */
  show: boolean;
  /** Колбэк, вызываемый при инициализации закрытия окна. */
  handleClose: () => void;
  /** Данные, которые ввёл пользователь. */
  data: FilmReviewPageData;
}

/** Компонент модального окна с поздравление пользователя об успешном создании рецензии. */
export default function SuccessModal({show, handleClose, data}: SuccessModalProps) {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      /* Disable animation in React.StrictMode due to using deprecated methods by component.
         Issue: https://github.com/react-bootstrap/react-bootstrap/issues/5075 */
      animation={false}
    >
      <Modal.Header closeButton>
        <Modal.Title>Congratulations!</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <ul>
          <li>Username: {data.username}</li>
          <li>Email: {data.email}</li>
          <li>Text: {data.text}</li>
        </ul>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
