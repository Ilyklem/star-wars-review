import { Form, Formik } from 'formik';
import React, { useCallback, useRef } from 'react';
import { useRouteMatch } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import * as Yup from 'yup';
import { Alert, Button, Card, Spinner } from 'react-bootstrap';
import SuccessModal from './components/SuccessModal';
import { FilmReviewPageStatus, reset, submit } from './filmReviewPageSlice';
import { createSelector } from '@reduxjs/toolkit';
import { selectFilmReviewPageState } from '../store';
import { useDispatch, useSelector } from 'react-redux';
import FormField from './components/FormField';
import { FormikProps } from 'formik/dist/types';

import styles from './FilmReviewPage.module.css';

/** Значение формы. */
interface FormValues {
  username: string;
  email: string;
  text: string;
}

/** Начальное значение формы. */
const formInitialValues: FormValues = {
  username: '',
  email: '',
  text: ''
};

/** Схема для валидации формы. */
const formValidationSchema = Yup.object().shape({
  username: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
  text: Yup.string().required('Required')
});

const selectStatus = createSelector([selectFilmReviewPageState], (state) => state.status);
const selectLastSubmittedData = createSelector([selectFilmReviewPageState], (state) => state.lastSubmittedData);

/** Компонент страницы для создания рецензии на фильм. */
export default function FilmReviewPage() {
  const match = useRouteMatch<{filmId: string}>();

  const formikHelpersRef = useRef<FormikProps<FormValues> | null>(null);

  const dispatch = useDispatch();
  const status = useSelector(selectStatus);
  const lastSubmittedData = useSelector(selectLastSubmittedData);

  const handleFormSubmit = useCallback((values: FormValues) => {
    dispatch(submit({filmId: match.params.filmId, data: values}));
  }, [match.params.filmId, dispatch]);

  const handleSuccessModalClose = useCallback(() => {
    dispatch(reset());
    formikHelpersRef.current?.resetForm();
  }, [dispatch]);

  const form = <Formik
    innerRef={formikHelpersRef}
    initialValues={formInitialValues}
    validationSchema={formValidationSchema}
    onSubmit={handleFormSubmit}
  >
    <Form>
      <FormField label="Username" name="username" disabled={status === FilmReviewPageStatus.Submitting}/>
      <FormField label="Email" name="email" disabled={status === FilmReviewPageStatus.Submitting}/>
      <FormField label="Text" name="text" inputType="textarea" disabled={status === FilmReviewPageStatus.Submitting}/>

      <div className={styles.actions}>
        <LinkContainer to={`/${match.params.filmId}`}>
          <Button className={styles.action} variant="secondary">Back</Button>
        </LinkContainer>

        <Button className={styles.action} type="submit" disabled={status === FilmReviewPageStatus.Submitting}>
          Submit
        </Button>

        {status === FilmReviewPageStatus.Submitting &&
          <Spinner className={styles.submissionIndicator} animation="border" variant="primary" />
        }
      </div>
    </Form>
  </Formik>;

  return <>
    <Card className={styles.card}>
      <Card.Body>
        <Card.Title>Review</Card.Title>
        {form}
        {status === FilmReviewPageStatus.Failed && <Alert className={styles.alert} variant="danger">Unknown error</Alert>}
      </Card.Body>
    </Card>

    {lastSubmittedData !== null &&
      <SuccessModal show={status === FilmReviewPageStatus.Done} handleClose={handleSuccessModalClose} data={lastSubmittedData}/>
    }
  </>;
}
