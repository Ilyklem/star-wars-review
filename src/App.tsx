import React, { Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { Provider as ReduxProvider } from 'react-redux';
import store from './store';

const FilmsPage = React.lazy(() => import('./FilmsPage/FilmsPage'));
const FilmReviewPage = React.lazy(() => import('./FilmReviewPage/FilmReviewPage'));

const loader = 'Loading…';

/** Компонент приложения для создания рецензий на фильмы. */
function App() {
  return (
    <ReduxProvider store={store}>
      <BrowserRouter>
        <Container className="p-3">
          <Suspense fallback={loader}>
            <Switch>
              <Route path="/:filmId/review">
                <FilmReviewPage/>
              </Route>
              <Route path="/:filmId?">
                <FilmsPage/>
              </Route>
            </Switch>
          </Suspense>
        </Container>
      </BrowserRouter>
    </ReduxProvider>
  );
}

export default App;
